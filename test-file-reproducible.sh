#!/usr/bin/env bash
set -eou pipefail
IFS=$'\n\t'

input_mdf=$1
output_cbf=$(mktemp)
recreated_mdf=$(mktemp)

function cleanup {
    rm -f "${output_cbf}" "${recreated_mdf}"
}
trap cleanup EXIT

set -x

./do_conversion.py to-cbf "${input_mdf}" "${output_cbf}"

./do_conversion.py read-cbf "${output_cbf}"

./do_conversion.py to-mdf "${output_cbf}" ${recreated_mdf}

du -Ahs "${input_mdf}" "${output_cbf}" ${recreated_mdf}

du -As "${input_mdf}" "${output_cbf}" ${recreated_mdf}

openssl sha256 "${input_mdf}" ${recreated_mdf}
