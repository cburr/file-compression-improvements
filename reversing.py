import base64
import enum
import struct
import subprocess
import uuid
from collections import defaultdict

import xxhash
import zstandard
from tqdm import tqdm

import decoding
import encoding
from lhcb_constants import BankType, CLID, OdinOffsets

HEADER_SIZE = 8 + 16 + 4 + 8
DEFAULT_DECOMPRESSOR = zstandard.ZstdDecompressor(format=zstandard.FORMAT_ZSTD1_MAGICLESS)


class DstDataCompressionAlgorithm(enum.StrEnum):
    NONE = enum.auto()
    ZSTD = enum.auto()


class Emulatable2024Streams(enum.StrEnum):
    TURBO = enum.auto()
    TURBORAW = enum.auto()
    FULL = enum.auto()


TURBO_BANK_TYPES_2023 = {
    BankType.ODIN,
    BankType.HltDecReports,
    BankType.HltRoutingBits,
    BankType.HltLumiSummary,
    BankType.HltSelReports,
}
FULL_BANK_TYPES_2023 = {
    BankType.ODIN,
    BankType.Rich,
    BankType.Plume,
    BankType.VPRetinaCluster,
    BankType.Calo,
    BankType.FTCluster,
    BankType.HltDecReports,
    BankType.Muon,
    BankType.HltRoutingBits,
    BankType.HltSelReports,
    BankType.HltLumiSummary,
}
TURBORAW_BANK_TYPES_2023 = {
    BankType.Rich,
    BankType.ODIN,
    BankType.Plume,
    BankType.VPRetinaCluster,
    BankType.Calo,
    BankType.FTCluster,
    BankType.HltDecReports,
    BankType.Muon,
    BankType.HltRoutingBits,
    BankType.HltSelReports,
    BankType.HltLumiSummary,
}

TURBO_BANK_TYPES_2024 = {
    BankType.ODIN,
    BankType.HltRoutingBits,
    BankType.HltLumiSummary,
    BankType.HltSelReports,
}
FULL_BANK_TYPES_2024 = TURBO_BANK_TYPES_2024.copy()
TURBORAW_BANK_TYPES_2024 = {x for x in TURBORAW_BANK_TYPES_2023 if x not in {BankType.HltDecReports}}


def build_old_raw_bank(raw_bank_type, raw_bank_version, raw_bank_source_id, bank_data):
    old_header = decoding.RAW_BANK_HEADER.pack(
        0xcbcb, len(bank_data) + decoding.RAW_BANK_HEADER.size, raw_bank_type, raw_bank_version, raw_bank_source_id
    )
    padding = b"\x00" * (-(len(old_header) + len(bank_data)) % 4)
    # BankInfo(size, full_size, bank_type, version, source_id, header, xxx[8:size], xxx[size:full_size])
    return decoding.BankInfo(len(old_header)+len(bank_data), len(old_header)+len(bank_data)+len(padding), BankType(raw_bank_type), raw_bank_version, raw_bank_source_id, old_header, bank_data, padding)


def build_old_dstdata(class_id, loc_id, link_loc_ids, body):
    header = b"".join([
        class_id.to_bytes(4, byteorder="little", signed=False),
        loc_id.to_bytes(4, byteorder="little", signed=True),
        len(link_loc_ids).to_bytes(4, byteorder="little", signed=False),
    ] + [link_loc_id.to_bytes(4, byteorder="little", signed=True) for link_loc_id in link_loc_ids] + [
        len(body).to_bytes(4, byteorder="little", signed=False),
    ])
    return decoding.DstDataRecord(CLID(class_id), loc_id, link_loc_ids, header, body)


def compress_dstdata(dst_data):
    key = int.from_bytes(dst_data[:4], "little")
    compression = int.from_bytes(dst_data[4:5], "little") & 0x7
    if compression == 0:
        header = dst_data[:8]
        dst_data = dst_data[8:]
        source_size = len(dst_data)
        target_size = len(dst_data)
    elif compression == 4:
        if dst_data[8:11] == b"ZS\x01":
            assert len(dst_data) >= 17
            source_size = int.from_bytes(dst_data[11:14], "little")
            target_size = int.from_bytes(dst_data[14:17], "little")
            header = dst_data[:17]
            compressed_data = dst_data[17:]
        elif dst_data[8:11] == b"ZS\x02":
            assert len(dst_data) >= 19
            source_size = int.from_bytes(dst_data[11:15], "little")
            target_size = int.from_bytes(dst_data[15:19], "little")
            header = dst_data[:19]
            compressed_data = dst_data[19:]
        else:
            raise NotImplementedError(dst_data[8:11])

        dst_data = zstandard.ZstdDecompressor().decompress(compressed_data)

        assert source_size == len(compressed_data), (source_size, len(compressed_data))
        assert target_size == len(dst_data)
    else:
        raise NotImplementedError(int.from_bytes(dst_data[4:5], "little"))

    return key, source_size, target_size, header, dst_data


def reproducible_compress(x):
    zstd_exe = "/Users/cburr/mambaforge/envs/lhcb-file-size2/bin/zstd"
    return subprocess.check_output([zstd_exe, "-10", "--no-check", f"--stream-size={len(x)}"], input=x)


def repack_dstdata(dstdata_records, encoding_key, compression_algorithm: DstDataCompressionAlgorithm):
    x = b"".join(r.header + r.body for r in dstdata_records)
    target_size = len(x)

    if x == b"":
        yy = b"\x00\x00\x00\x00\x00\x00\x00\x00"
    else:
        key = encoding_key.to_bytes(4, byteorder="little", signed=False)
        match compression_algorithm:
            case DstDataCompressionAlgorithm.NONE:
                compression = int.to_bytes(0x0, 1, byteorder="little", signed=False)
                header = key + compression + b"\x00\x00\x00"
                y = x
            case DstDataCompressionAlgorithm.ZSTD:
                compression = int.to_bytes(0x4, 1, byteorder="little", signed=False)
                header = key + compression + b"\x00\x00\x00"
                y = reproducible_compress(x)
                source_size = len(y)
                if len(y) <= 0xffffff:
                    header += b"ZS\x01"
                    header += source_size.to_bytes(3, byteorder="little", signed=False)
                    header += target_size.to_bytes(3, byteorder="little", signed=False)
                else:
                    header += b"ZS\x02"
                    header += source_size.to_bytes(4, byteorder="little", signed=False)
                    header += target_size.to_bytes(4, byteorder="little", signed=False)
            case _:
                raise NotImplementedError(compression_algorithm)
        yy = header + y

    source_id = 0x0200

    while yy:
        yield build_old_raw_bank(BankType.DstData, 3, source_id, yy[:65532 - decoding.RAW_BANK_HEADER.size])
        yy = yy[65532 - decoding.RAW_BANK_HEADER.size:]
        source_id += 1


def read_header(fh):
    assert fh.read(8) == b"LHCbCBF\x01"
    guid = uuid.UUID(bytes=fh.read(16))
    n_events = int.from_bytes(fh.read(4), byteorder="little", signed=False)
    n_events_bytes = int.from_bytes(fh.read(8), byteorder="little", signed=False)
    assert fh.tell() == HEADER_SIZE
    return guid, n_events, n_events_bytes


def read_footer(fh, n_events):
    assert fh.read(8) == b"LHCbCBF\x00"
    n_compress_dicts = int.from_bytes(fh.read(4), byteorder="little", signed=False)
    hasher = xxhash.xxh64()
    decompressors = {}
    for _ in range(n_compress_dicts):
        raw_dict_size = fh.read(4)
        hasher.update(raw_dict_size)
        dict_data = fh.read(int.from_bytes(raw_dict_size, byteorder="little", signed=False))
        hasher.update(dict_data)
        compress_dict = zstandard.ZstdCompressionDict(dict_data)
        decompressors[compress_dict.dict_id()] = zstandard.ZstdDecompressor(format=zstandard.FORMAT_ZSTD1_MAGICLESS, dict_data=compress_dict)
    expected_checksum = int.from_bytes(fh.read(4), byteorder="little", signed=False)
    actual_checksum = int.from_bytes(hasher.digest()[:3:-1], byteorder="little", signed=False)
    assert actual_checksum == expected_checksum

    all_offsets = []
    all_dec_reports = []
    with DEFAULT_DECOMPRESSOR.stream_reader(fh, closefd=False) as zh:
        for n in range(n_events):
            offset = int.from_bytes(zh.read(8), byteorder="little", signed=False)
            n_dec_reports = int.from_bytes(zh.read(1), byteorder="little", signed=False)
            dec_reports = []
            for i in range(n_dec_reports):
                version = int.from_bytes(zh.read(1), byteorder="little", signed=False)
                source_id = int.from_bytes(zh.read(2), byteorder="little", signed=False)
                data_size = int.from_bytes(zh.read(4), byteorder="little", signed=False)
                dec_reports.append([version, source_id, zh.read(data_size)])
            all_offsets.append(offset)
            all_dec_reports.append(dec_reports)
        assert zh.read() == b""
    n_fsrs = int.from_bytes(fh.read(2), byteorder="little", signed=False)
    assert n_fsrs == 0, "TODO"

    remainder = fh.read()
    assert remainder == b"", len(remainder)

    return all_offsets, all_dec_reports, decompressors, actual_checksum


def include_bank(emulate_2024, raw_bank_type):
    match emulate_2024:
        case Emulatable2024Streams.TURBO:
            assert raw_bank_type in TURBO_BANK_TYPES_2023
            if raw_bank_type not in TURBO_BANK_TYPES_2024:
                return False
        case Emulatable2024Streams.TURBORAW:
            assert raw_bank_type in TURBORAW_BANK_TYPES_2023
            if raw_bank_type not in TURBORAW_BANK_TYPES_2024:
                return False
        case Emulatable2024Streams.FULL:
            assert raw_bank_type in FULL_BANK_TYPES_2023
            if raw_bank_type not in FULL_BANK_TYPES_2024:
                return False
        case None:
            pass
        case _:
            raise NotImplementedError(emulate_2024)
    return True


def read_cbf(cbf_path, *, mdf_out=None, side_channel=None, size_info=None, dstdata_compression_algorithm=DstDataCompressionAlgorithm.ZSTD, preserve_bank_order=True, emulate_2024: Emulatable2024Streams | None=None):
    if mdf_out is not None and side_channel is None:
        raise ValueError("side_channel is required when mdf_out is provided")

    mdf_out_fh = mdf_out.open("wb") if mdf_out else None
    if size_info is not None:
        size_info["header_size"] = HEADER_SIZE
        size_info["footer"] = {}
        size_info["events"] = []
    try:
        with cbf_path.open(mode="rb") as fh, tqdm(unit="MiB", unit_scale=1/1024**2) as pbar:
            # HEADER
            guid, n_events, n_events_bytes = read_header(fh)

            # FOOTER
            fh.seek(HEADER_SIZE + n_events_bytes)
            all_offsets, all_dec_reports, decompressors, actual_checksum = read_footer(fh, n_events)
            if size_info:
                size_info["footer"]["compressed"] = fh.tell() - HEADER_SIZE - n_events_bytes

            # EVENTS
            fh.seek(HEADER_SIZE)
            pbar.reset(total=n_events_bytes)
            for n_event, (offset, next_offset, dec_reports) in enumerate(zip(all_offsets, all_offsets[1:] + [n_events_bytes], all_dec_reports), start=1):
                if size_info:
                    size_info["events"].append({
                        "n_event": n_event,
                        "raw_bank_sizes": defaultdict(lambda: {"compressed": 0, "uncompressed": 0, "n_banks": 0}),
                        "reco_bank_sizes": defaultdict(lambda: {"compressed": 0, "uncompressed": 0, "n_banks": 0}),
                    })

                # print(i, offset)
                event_data = fh.read(next_offset - offset)
                assert len(event_data) >= 4
                expected_checksum = event_data[-4:]
                event_data = event_data[:-4]
                actual_checksum = xxhash.xxh64_digest(event_data)[:3:-1]
                assert actual_checksum == expected_checksum

                raw_banks = []
                dstdata_records = []
                current_offset = 0
                while len(event_data) > current_offset:
                    bank_metadata, = struct.unpack_from('B', event_data, current_offset)
                    compact_bank_type = bank_metadata & 0b00000011
                    assert bank_metadata & 0b00001100 == 0, bin(bank_metadata)
                    use_byte_shuffle = bank_metadata & 0b10000000

                    if compact_bank_type == 0:
                        raw_bank_type, raw_bank_version = struct.unpack_from('BB', event_data, current_offset + 1)
                        frame_params = zstandard.get_frame_parameters(b"\x28\xB5\x2F\xFD" + event_data[current_offset+3 : current_offset+3+14])
                        d = decompressors.get(frame_params.dict_id, DEFAULT_DECOMPRESSOR).decompressobj()
                        grouped_bank_data = d.decompress(event_data[current_offset+3:])
                        if size_info:
                            key = f"{BankType(raw_bank_type).name}-v{raw_bank_version}"
                        while grouped_bank_data:
                            size, raw_bank_source_id = struct.unpack_from('<IH', grouped_bank_data, 0)
                            bank_data = grouped_bank_data[6:4+size]
                            assert len(grouped_bank_data) >= 4+size
                            grouped_bank_data = grouped_bank_data[4+size:]
                            if not include_bank(emulate_2024, raw_bank_type):
                                continue
                            raw_banks.append((raw_bank_type, raw_bank_version, raw_bank_source_id, bank_data))
                            if size_info:
                                size_info["events"][-1]["raw_bank_sizes"][key]["uncompressed"] += 8+size-2 + (-(8+size-2) % 4)
                                size_info["events"][-1]["raw_bank_sizes"][key]["n_banks"] += 1
                        if size_info:
                            size_info["events"][-1]["raw_bank_sizes"][key]["compressed"] += len(event_data) - len(d.unused_data)
                        event_data = d.unused_data
                    elif compact_bank_type == 1:
                        class_id, = struct.unpack_from('I', event_data, current_offset+1)
                        frame_params = zstandard.get_frame_parameters(b"\x28\xB5\x2F\xFD" + event_data[current_offset+5:current_offset+5+14])
                        d = decompressors.get(frame_params.dict_id, DEFAULT_DECOMPRESSOR).decompressobj()
                        grouped_bank_data = d.decompress(event_data[current_offset+5:])
                        if size_info:
                            key = f"{CLID(class_id).name}"
                        while grouped_bank_data:
                            size, loc_id, n_link_loc_ids = struct.unpack('<IiI', grouped_bank_data[0:12])
                            link_loc_ids_format = '<' + 'i' * n_link_loc_ids
                            link_loc_ids = list(struct.unpack_from(link_loc_ids_format, grouped_bank_data, 12))

                            assert len(grouped_bank_data) >= 4+size
                            body = grouped_bank_data[12+4*n_link_loc_ids:4+size]
                            grouped_bank_data = grouped_bank_data[4+size:]
                            dstdata_records.append((class_id, loc_id, link_loc_ids, body))
                            if size_info:
                                size_info["events"][-1]["reco_bank_sizes"][key]["uncompressed"] += 4+size
                                size_info["events"][-1]["reco_bank_sizes"][key]["n_banks"] += 1
                        if size_info:
                            size_info["events"][-1]["reco_bank_sizes"][key]["compressed"] += len(event_data) - len(d.unused_data)
                        event_data = d.unused_data
                    else:
                        raise NotImplementedError(compact_bank_type)
                    current_offset = len(event_data) - len(d.unused_data)

                for type_version, source_id, bank_data in dec_reports:
                    bank_version = type_version & encoding.BANK_VERSION_MASK
                    bank_type = encoding.REVERSE_SUMMARY_TYPE_MAPPING[type_version >> 5]
                    if not include_bank(emulate_2024, bank_type):
                        continue
                    raw_banks.append((bank_type, bank_version, source_id, bank_data))
                    if size_info:
                        key = f"{bank_type.name}-v{bank_version}"
                        # The HltDecReports are stored as part of the overheads, so we don't count them in the raw bank sizes
                        size_info["events"][-1]["raw_bank_sizes"][key]["uncompressed"] += 8 + len(bank_data)
                        size_info["events"][-1]["raw_bank_sizes"][key]["n_banks"] += 1

                if mdf_out:
                    old_raw_banks = [build_old_raw_bank(*x) for x in raw_banks]
                    if dstdata_records or side_channel[n_event-1].get("encoding_key") is not None:
                        old_dstdata_records = [build_old_dstdata(*x) for x in dstdata_records]
                        old_raw_banks.extend(repack_dstdata(
                            old_dstdata_records, side_channel[n_event-1]["encoding_key"], dstdata_compression_algorithm
                        ))

                    if preserve_bank_order:
                        # Get the raw banks in the same order
                        banks_by_hash = {xxhash.xxh64_hexdigest(r.header + r.data + r.padding): r for r in old_raw_banks}
                        try:
                            old_raw_banks = [banks_by_hash[x] for x in side_channel[n_event-1]["bank_positions"]]
                        except KeyError:
                            print(f"Missing bank in event {n_event} of {cbf_path}")
                            raise

                    size, _, _, checksum, misc, event_mask, run_number, orbit_number, bunch_id = decoding.MDF_HEADER.unpack(
                        base64.b64decode(side_channel[n_event-1]["mdf_header"])
                    )

                    routing_bits_banks = [x for x in raw_banks if x[0] == BankType.HltRoutingBits]
                    assert len(routing_bits_banks) == 1
                    assert event_mask == routing_bits_banks[0][-1] + b"\xff\xff\xff\xff"

                    odin_banks = [x for x in raw_banks if x[0] == BankType.ODIN]
                    assert len(odin_banks) == 1
                    odin_data = int.from_bytes(odin_banks[0][-1], "little")
                    assert run_number == (odin_data >> OdinOffsets.RunNumberOffset) & (2**OdinOffsets.RunNumberSize - 1)
                    assert orbit_number == (odin_data >> OdinOffsets.OrbitNumberOffset) & (2**OdinOffsets.OrbitNumberSize - 1)
                    assert bunch_id == (odin_data >> OdinOffsets.BunchIdOffset) & (2**OdinOffsets.BunchIdSize - 1)

                    repacked_mdf_payload = b"".join(r.header + r.data + r.padding for r in old_raw_banks)
                    size = decoding.MDF_HEADER.size + len(repacked_mdf_payload)
                    repacked_mdf_header = decoding.MDF_HEADER.pack(size, size, size, checksum, misc, event_mask, run_number, orbit_number, bunch_id)
                    mdf_out_fh.write(repacked_mdf_header + repacked_mdf_payload)
                pbar.update(next_offset - offset)
    finally:
        if mdf_out:
            mdf_out_fh.close()
