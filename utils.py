from collections import namedtuple

import numpy as np


BankInfo = namedtuple(
    "BankInfo", ["size", "full_size", "type", "version", "source_id", "header", "data", "padding"]
)


def delta_encode_obj(obj: dict, fields):
    encoded = {
        k: v if k not in fields else delta_encode(v)
        for k, v in obj.items()
    }
    assert obj == {
        k: v if k not in fields else list(np.cumsum(v))
        for k, v in encoded.items()
    }
    return encoded


def delta_encode(v):
    return (v[:1] + list(np.array(v)[1:] - np.array(v)[:-1]))


def byte_shuffle(x):
    return x[0::4] + x[1::4] + x[2::4] + x[3::4]


def bhex(data):
    return "0x" + "".join(map(lambda x: hex(x)[2:].rjust(2, "0"), data))


def group_banks(bank_infos):
    previous_bank_types = set()
    previous_banks = []
    for size, full_size, bank_type, *args in bank_infos:
        if len(set(previous_bank_types) | {bank_type}) == 2:
            yield previous_bank_types.pop(), previous_banks
            previous_banks = []
        previous_bank_types.add(bank_type)
        previous_banks.append(BankInfo(size, full_size, bank_type, *args))
    yield previous_bank_types.pop(), previous_banks
    assert not previous_bank_types
