#!/usr/bin/env bash
set -eou pipefail
IFS=$'\n\t'

input_mdf=$(realpath "$1")
stream=$2
output_fn=$PWD/$3
output_dir=$(mktemp -d)
this_dir=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")

function do_conversion() {
    "${this_dir}/do_conversion.py" "$@"
    return $?
}

function compress_mdf_on_lblhcbpr20() {
    in_fn=$1
    out_fn=$2
    rsync "${in_fn}" lblhcbpr20:/tmp/cburr/test.mdf
    ssh -t lblhcbpr20 bash -lc 'pwd; cd ~/InputSandbox792825310 && ../stack2/LHCb/run lbexec GaudiConf.mergeMDF:mdf lbexec_options_00196356_00001423_1.yaml' 2>&1 | tail -n 15
    rsync lblhcbpr20:InputSandbox792825310/00196356_00001423_1.turboraw.raw.tmp2 "${out_fn}"
    return $?
}

function cleanup {
    rm -rf "${output_dir}"
}
trap cleanup EXIT

# Make sure we can connect to lblhcbpr20
ssh -t lblhcbpr20 echo

set -x

cp "${input_mdf}" "${output_dir}/00-current.mdf"

cd "${output_dir}"

do_conversion to-cbf "00-current.mdf" "01-current.cbf" --compress-level 10 --compression-dicts-training-size -1

do_conversion to-mdf "01-current.cbf" "02-uncompressed.mdf" --no-preserve-bank-order --dstdata-compression-algorithm none

zstd -10 "02-uncompressed.mdf" -o "02a-current.mdf.zst"

compress_mdf_on_lblhcbpr20 "02-uncompressed.mdf" "03-event-compressed.mdf"

do_conversion to-mdf "01-current.cbf" "06-expected-dst-compressed.mdf" --no-preserve-bank-order --emulate-2024 "${stream}"

do_conversion to-mdf "01-current.cbf" "05-expected-uncompressed.mdf" --no-preserve-bank-order --dstdata-compression-algorithm none --emulate-2024 "${stream}"

zstd -10 "05-expected-uncompressed.mdf" -o "05a-expected.mdf.zst"

compress_mdf_on_lblhcbpr20 "05-expected-uncompressed.mdf" "04-compressed.mdf"

do_conversion to-cbf "06-expected-dst-compressed.mdf" "07-expected.cbf" --compress-level 10 --compression-dicts-training-size -1

do_conversion strip-overhead "07-expected.cbf" "08-expected-no-overhead.cbf"

do_conversion to-cbf "06-expected-dst-compressed.mdf" "09-expected-only-physics.cbf" --only-physics --compress-level 10 --compression-dicts-training-size -1

do_conversion strip-overhead "09-expected-only-physics.cbf" "10-expected-only-physics-no-overhead.cbf"

du -Ak $(find . -name '*.cbf' -o -name '*.mdf' -o -name '*.mdf.zst' | sort) | \
    awk '
        NR == 1 { ref1 = $1 }
        NR == 6 { ref2 = $1 }
        NR >= 1 && NR <= 5 { ratio = ref1 / $1; printf "  %6d (%s) %s\n", $1, (ratio < 1 ? sprintf("%.3f", ratio) : sprintf("%.2f", ratio)), $2}
        NR >= 6 { ratio = ref2 / $1; printf "  %6d (%s) %s\n", $1, (ratio < 1 ? sprintf("%.3f", ratio) : sprintf("%.2f", ratio)), $2 }
    ' | \
    tee "${output_fn}"
