import base64
import struct
from collections import defaultdict, namedtuple
from functools import partial, lru_cache
from pathlib import Path
from typing import Iterator

import requests
import xxhash
import zstandard
from tqdm import tqdm

from lhcb_constants import BankType, CLID


BankInfo = namedtuple(
    "BankInfo", ["size", "full_size", "type", "version", "source_id", "header", "data", "padding"]
)
DstDataRecord = namedtuple("DstDataRecord", ["class_id", "loc_id", "link_loc_ids", "header", "body"])
OldLHCbEvent = namedtuple("LHCbEvent",
    ["mdf_header", "raw_banks", "dstdata_records", "is_physics", "encoding_key", "dst_data_pos"]
)


def is_physics(event_mask):
    routing_bits = int.from_bytes(event_mask, "little")
    # is_lumi = (routing_bits >> 94) & 1
    return (routing_bits >> 95) & 1


def read_mdf(path: Path, *, progress=True, evt_max=None):
    if progress is True:
        with tqdm(total=path.stat().st_size, unit="MiB", unit_scale=1/1024**2) as progress:
            yield from read_mdf(path=path, progress=progress, evt_max=evt_max)
        return

    with path.open("rb") as fh:
        i = 0
        while True:
            if progress:
                progress.update(fh.tell() - progress.n)
            i += 1
            result = read_mdf_event(fh)
            if result is None:
                break
            size, checksum, misc, event_mask, run_number, orbit_number, bunch_id, raw_data = result
            yield i, size, checksum, misc, event_mask, run_number, orbit_number, bunch_id, raw_data

            if evt_max is not None and i >= evt_max:
                break


MDF_HEADER = struct.Struct("<III4s4s16sIII")


def read_mdf_event(fh):
    header = fh.read(MDF_HEADER.size)
    if header == b"":
        return None
    size, b, c, checksum, misc, event_mask, run_number, orbit_number, bunch_id = MDF_HEADER.unpack(header)
    # misc = Spare/Data Type/HdrTyp/Comp
    if size != b or size != c:
        raise NotImplementedError(fh.tell(), size, b, c)
    raw_data = header + fh.read(size-MDF_HEADER.size)
    # https://gitlab.cern.ch/lhcb-online/lhcb-online-mover/-/blob/219b392e2f713ddbe2680543608bbcd9631643c6/src/lbMover/routing.py#L21-46
    routing_bits = int.from_bytes(event_mask, "little")
    is_lumi = (routing_bits >> 94) & 1
    is_physics = (routing_bits >> 95) & 1
    return size, checksum, misc, event_mask, run_number, orbit_number, bunch_id, raw_data


RAW_BANK_HEADER = struct.Struct("<HHBBH")

def decode_event(raw_data):
    xxx = raw_data[4*12:]
    while xxx:
        header = xxx[:8]
        magic, size, bank_type, version, source_id = RAW_BANK_HEADER.unpack(header)
        assert magic == 0xcbcb

        full_size = size + (4 - size % 4 if size % 4 else 0)
        bank_type = BankType(bank_type)

        yield BankInfo(size, full_size, bank_type, version, source_id, header, xxx[8:size], xxx[size:full_size])

        # assert xxx[0:2] == b"\xcb\xcb"
        # size = int.from_bytes(xxx[2:4], "little")
        # full_size = size + (4 - size % 4 if size % 4 else 0)
        # bank_type = BankType(int.from_bytes(xxx[4:5], "little"))
        # version = int.from_bytes(xxx[5:6], "little")
        # source_id = int.from_bytes(xxx[6:8], "little", signed=True)
        # # print(i, bank_type, bank_type.name, version, source_id, full_size)  # , *map(hex, xxx[:full_size+2]))
        # # yield size, full_size, bank_type, version, source_id, xxx[:full_size]
        # yield size, full_size, bank_type, version, source_id, header, xxx[8:size], xxx[size:full_size]

        xxx = xxx[full_size:]


def decompress_dstdata(data):
    key = int.from_bytes(data[:4], "little")
    compression = int.from_bytes(data[4:5], "little") & 0x7
    if compression == 0:
        header = data[:8]
        dst_data = data[8:]
        source_size = len(dst_data)
        target_size = len(dst_data)
    elif compression == 4:
        if data[8:11] == b"ZS\x01":
            assert len(data) >= 17
            source_size = int.from_bytes(data[11:14], "little")
            target_size = int.from_bytes(data[14:17], "little")
            header = data[:17]
            compressed_data = data[17:]
        elif data[8:11] == b"ZS\x02":
            assert len(data) >= 19
            source_size = int.from_bytes(data[11:15], "little")
            target_size = int.from_bytes(data[15:19], "little")
            header = data[:19]
            compressed_data = data[19:]
        else:
            raise NotImplementedError(data[8:11])

        dst_data = zstandard.ZstdDecompressor().decompress(compressed_data)

        assert source_size == len(compressed_data), (source_size, len(compressed_data))
        assert target_size == len(dst_data)
    else:
        raise NotImplementedError(int.from_bytes(data[4:5], "little"))

    return key, source_size, target_size, header, dst_data


def decode_dstdata(dst_data):
    while dst_data:
        class_id = CLID(int.from_bytes(dst_data[:4], "little", signed=False))
        location_id = int.from_bytes(dst_data[4:8], "little", signed=True)
        link_location_ids_size, link_location_ids = load_vector(dst_data[8:])

        header_size = 16 + link_location_ids_size*4
        stored_size = int.from_bytes(dst_data[header_size-4:header_size], "little", signed=False)
        assert len(dst_data) >= header_size + stored_size

        yield DstDataRecord(
            class_id, location_id, link_location_ids, dst_data[:header_size], dst_data[header_size:header_size+stored_size]
        )
        dst_data = dst_data[header_size + stored_size:]
        # print(
        #     class_id, class_id.name, location_id, link_location_ids_size, link_location_ids,
        #     stored_size, f"{(header_size + stored_size)/full_size:.2%}"
        # )


@lru_cache(1024)
def get_packed_location_map(key):
    key = hex(key)[2:]
    url = f"https://gitlab.cern.ch/lhcb-conddb/file-content-metadata/-/raw/commissioning/ann/json/{key[:2]}/{key}.json"
    r = requests.get(url, timeout=15)
    r.raise_for_status()
    return {int(k): v for k, v in r.json()["PackedObjectLocations"].items()}


def load_vector(data):
    size = int.from_bytes(data[:4], "little", signed=False)
    vector = [
        int.from_bytes(_nonempty(data[4 + i*4:8 + i*4]), "little", signed=True)
        for i in range(size)
    ]
    return size, vector


def _nonempty(x):
    assert len(x) > 0
    return x

def _array_unpack(data, n_records, *, fmt, check_overflow=True):
    offset = struct.calcsize(fmt) * n_records
    if len(data) < offset:
        msg = f"Buffer overflow!!! {len(data)=} {offset=}"
        if check_overflow:
            raise Exception(msg)
        print(msg)
    result = [x[0] for x in struct.iter_unpack(fmt, data[:offset])]
    return offset, result


ARRAY_UNPACKERS = {
    "std::int16_t": partial(_array_unpack, fmt="<h"),
    "std::uint16_t": partial(_array_unpack, fmt="<H"),
    "std::int32_t": partial(_array_unpack, fmt="<i"),
    "std::uint32_t": partial(_array_unpack, fmt="<I"),
    "std::int64_t": partial(_array_unpack, fmt="<q"),
    "std::uint64_t": partial(_array_unpack, fmt="<Q"),
    "float": partial(_array_unpack, fmt="<f"),
    "half float": partial(_array_unpack, fmt="<e"),
}

ARRAY_PACKERS = {
    "std::int16_t": lambda x: [struct.pack("<h", x_) for x_ in x],
    "std::uint16_t": lambda x: [struct.pack("<H", x_) for x_ in x],
    "std::int32_t": lambda x: [struct.pack("<i", x_) for x_ in x],
    "std::uint32_t": lambda x: [struct.pack("<I", x_) for x_ in x],
    "std::int64_t": lambda x: [struct.pack("<q", x_) for x_ in x],
    "std::uint64_t": lambda x: [struct.pack("<Q", x_) for x_ in x],
    "float": lambda x: [struct.pack("<f", x_) for x_ in x],
    "half float": lambda x: [struct.pack("<e", x_) for x_ in x],
}


def read_old_events(input_path, *, evt_max=None, first_evt=None, verbose=False, fsrs=None, side_channel=None, size_info=None, only_physics=False) -> Iterator[OldLHCbEvent]:
    if size_info is not None:
        size_info["events"] = []

    for n_event, size, checksum, misc, event_mask, run_number, orbit_number, bunch_id, raw_data in read_mdf(input_path, progress=True, evt_max=evt_max):
        if first_evt and n_event < first_evt:
            continue
        if only_physics and not is_physics(event_mask):
            continue
        yield process_event(n_event, raw_data, is_physics_event=is_physics(event_mask), verbose=verbose, side_channel=side_channel, size_info=size_info)

    
def process_event(n_event, raw_data, *, is_physics_event, verbose=False, side_channel=None, size_info=None) -> OldLHCbEvent:
    if size_info:
        size_info["events"].append({
            "n_event": n_event,
            "mdf_bank_sizes": defaultdict(lambda: {"size": 0, "n_banks": 0}),
        })

    raw_banks = []
    if side_channel is not None:
        side_channel.append({
            "bank_positions": [],
            "dstdata_positions": [],
            "mdf_header": base64.b64encode(raw_data[:4*12]).decode()
        })
    dst_data_banks = []
    dst_data_pos = None
    dst_data_src_id = None
    for bank_info in decode_event(raw_data):
        if bank_info.type == BankType.DstData:
            if dst_data_pos is None:
                dst_data_pos = raw_data.index(bank_info.header + bank_info.data + bank_info.padding)
            if dst_data_src_id is None:
                assert bank_info.source_id in {256, 512, 768}
                dst_data_src_id = bank_info.source_id
            else:
                assert dst_data_src_id ==  bank_info.source_id, (dst_data_src_id, bank_info.source_id)
            dst_data_src_id += 1
            dst_data_banks.append(bank_info.data)
        else:
            raw_banks.append(bank_info)
        if side_channel is not None:
            bank_hash = xxhash.xxh64_hexdigest(bank_info.header + bank_info.data + bank_info.padding)
            side_channel[-1]["bank_positions"].append(bank_hash)
            if verbose:
                print("Raw", bank_hash, repr(bank_info.type), bank_info.source_id)
        if size_info:
            key = f"{bank_info.type.name}-v{bank_info.version}"
            size_info["events"][-1]["mdf_bank_sizes"][key]["size"] += bank_info.full_size
            size_info["events"][-1]["mdf_bank_sizes"][key]["n_banks"] += 1

    encoding_key = None
    dstdata_records = []
    if dst_data_banks:
        encoding_key, source_size, target_size, dstdata_header, dst_data = decompress_dstdata(b"".join(dst_data_banks))
        if side_channel is not None:
            side_channel[-1]["encoding_key"] = encoding_key
        for record in decode_dstdata(dst_data):
            dstdata_records.append(record)
            if side_channel is not None:
                record_hash = xxhash.xxh64_hexdigest(record.header + record.body)
                side_channel[-1]["dstdata_positions"].append(record_hash)
                if verbose:
                    print("DstData", record_hash, repr(record.class_id), record.loc_id)

    return OldLHCbEvent(
        raw_data[:4*12], raw_banks, dstdata_records, is_physics_event, encoding_key, dst_data_pos
    )
