# Proof of concept for optimization of LHCb files

This repository contains a crude proof of concept showing how LHCb data can be more efficiently stored.

## Usage

To benchmark the conversion of a MDF run:

```bash
./test-file.sh ../data/rmatev/hlt2_test_269939_2/stream_93.mdf
```
