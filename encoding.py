from collections import defaultdict, namedtuple

import uuid
import xxhash
import zstandard
from itertools import chain
from tqdm import tqdm

from lhcb_constants import BankType


EventIndexRecord = namedtuple("EventIndexRecord", ["offset", "dec_reports"])


BANK_VERSION_MASK = 0b00011111
SUMMARY_TYPE_MAPPING = {
    BankType.HltDecReports: 0,
    BankType.HltSelReports: 1,
    BankType.HltRoutingBits: 2,
    BankType.ODIN: 3,
}
REVERSE_SUMMARY_TYPE_MAPPING = dict(zip(*list(zip(*SUMMARY_TYPE_MAPPING.items()))[::-1]))


def group_raw_records(raw_banks):
    dec_reports = []
    grouped_records = defaultdict(list)
    for raw_bank in raw_banks:
        if raw_bank.type in (
            BankType.HltDecReports,
            BankType.HltSelReports,
            BankType.HltRoutingBits,
            BankType.ODIN,
        ):
            assert raw_bank.version <= BANK_VERSION_MASK
            type_version = raw_bank.version | (SUMMARY_TYPE_MAPPING[raw_bank.type] << 5)
            packed = b"".join([
                type_version.to_bytes(length=1, byteorder="little", signed=False),
                # TODO: Move source ID to type_version
                raw_bank.source_id.to_bytes(length=2, byteorder="little", signed=False),
                len(raw_bank.data).to_bytes(length=4, byteorder="little", signed=False),
                raw_bank.data,
            ])
            dec_reports.append(packed)
        else:
            record = b"".join([
                raw_bank.source_id.to_bytes(length=2, byteorder="little", signed=False),
                raw_bank.data,
            ])
            grouped_records[(raw_bank.type, raw_bank.version)].extend([
                len(record).to_bytes(length=4, byteorder="little", signed=False),
                record,
            ])
    return dec_reports, grouped_records


def group_reco_records(dstdata_records):
    grouped_records = defaultdict(list)
    for dstdata_record in dstdata_records:
        record = b"".join([
            dstdata_record.loc_id.to_bytes(length=4, byteorder="little", signed=True),
            len(dstdata_record.link_loc_ids).to_bytes(length=4, byteorder="little", signed=False),
            *[x.to_bytes(length=4, byteorder="little", signed=True) for x in dstdata_record.link_loc_ids],
            dstdata_record.body,
        ])
        grouped_records[dstdata_record.class_id].extend([
            len(record).to_bytes(length=4, byteorder="little", signed=False),
            record,
        ])
    return grouped_records


def build_compressors(old_events, *, compress_level: int):
    params = dict(
        format=zstandard.FORMAT_ZSTD1_MAGICLESS,
        write_checksum=False,
        write_dict_id=True,
    )
    default_compressor = zstandard.ZstdCompressor(
        compression_params=zstandard.ZstdCompressionParameters.from_level(compress_level, **params)
    )

    dict_training_data = defaultdict(list)
    n_physics = 0
    n_other = 0
    for event in old_events:
        if event.is_physics:
            n_physics += 1
        else:
            n_other += 1
        _, grouped_raw_banks = group_raw_records(event.raw_banks)
        for key, records in grouped_raw_banks.items():
            dict_training_data[key].append(b"".join(records))
        for class_id, records in group_reco_records(event.dstdata_records).items():
            dict_training_data[class_id].append(b"".join(records))

    compression_dicts = {}
    compressors = {}
    DICT_SIZE = 100*1024
    for i, (key, training_data) in enumerate(dict_training_data.items(), start=1):
        source_size = max(map(len, training_data))
        if (training_data_size := sum(map(len, training_data))) < DICT_SIZE:
            # print(f"Not training for {key!r} as too little training data ({training_data_size} bytes)")
            dictionary = None
        else:
            dictionary = zstandard.train_dictionary(DICT_SIZE, training_data, level=compress_level)
            compression_dicts[key] = dictionary
        compressors[key] = zstandard.ZstdCompressor(
            dict_data=dictionary,
            compression_params=zstandard.ZstdCompressionParameters.from_level(compress_level, **params, source_size=source_size),
        )

    total_dict_size = sum(len(d.as_bytes()) for d in compression_dicts.values())
    print(f"Trained {len(compression_dicts)} dictionaries ({total_dict_size / 1024:.3f} KiB) on {n_physics} physics events and {n_other} other events")
    return default_compressor, compressors, compression_dicts


def write_new_events(output_path, old_events, fsrs, *, compress_level, compression_dicts_training_size):
    training_events = []
    if compression_dicts_training_size:
        for event in old_events:
            training_events.append(event)
            if compression_dicts_training_size > 0 and len(training_events) >= compression_dicts_training_size:
                break
    default_compressor, compressors, compression_dicts = build_compressors(training_events, compress_level=compress_level)

    event_index = []
    with output_path.open(mode="wb") as fh:
        # HEADER
        # magic and version
        fh.write(b"LHCbCBF\x01")
        # guid
        fh.write(uuid.uuid4().bytes)
        # n_events and n_events_bytes
        fh.write(int.to_bytes(0, length=4, byteorder="little", signed=False))
        fh.write(int.to_bytes(0, length=8, byteorder="little", signed=False))

        # EVENTS
        offset = 0
        n_event = 0
        for event in tqdm(chain(training_events, old_events)):
            n_event += 1
            dec_reports, grouped_raw_banks = group_raw_records(event.raw_banks)
            event_index.append(EventIndexRecord(offset=offset, dec_reports=dec_reports))
            hasher = xxhash.xxh64()
            write = lambda x: hasher.update(x) or fh.write(x)

            for (raw_bank_type, raw_bank_version), records in grouped_raw_banks.items():
                bank_data = b"".join([
                    int.to_bytes(0b00000000, length=1, byteorder="little", signed=False),
                    raw_bank_type.to_bytes(length=1, byteorder="little", signed=False),
                    raw_bank_version.to_bytes(length=1, byteorder="little", signed=False),
                ])
                offset += write(bank_data)
                raw_records = b"".join(records)
                compressed_records = compressors.get((raw_bank_type, raw_bank_version), default_compressor).compress(raw_records)
                offset += write(compressed_records)

            for class_id, records in group_reco_records(event.dstdata_records).items():
                bank_data = b"".join([
                    int.to_bytes(0b00000001, length=1, byteorder="little", signed=False),
                    class_id.to_bytes(length=4, byteorder="little", signed=False),
                ])
                offset += write(bank_data)
                raw_records = b"".join(records)
                compressed_records = compressors.get(class_id, default_compressor).compress(raw_records)
                offset += write(compressed_records)

            offset += fh.write(hasher.digest()[:3:-1])

        # FOOTER
        # magic
        fh.write(b"LHCbCBF\x00")
        # n_compress_dicts
        fh.write(len(compression_dicts).to_bytes(length=4, byteorder="little", signed=False))
        # compress_dicts
        hasher = xxhash.xxh64()
        for compress_dict in compression_dicts.values():
            compress_dict_data = compress_dict.as_bytes()
            compress_dict_data = len(compress_dict_data).to_bytes(length=4, byteorder="little", signed=False) + compress_dict_data
            hasher.update(compress_dict_data)
            fh.write(compress_dict_data)
        # dicts_checksum
        fh.write(hasher.digest()[:3:-1])
        # event_index
        event_index_data = []
        for record in event_index:
            event_index_data.append(record.offset.to_bytes(length=8, byteorder="little", signed=False))
            event_index_data.append(len(record.dec_reports).to_bytes(length=1, byteorder="little", signed=False))
            event_index_data.extend(record.dec_reports)
        compressed_event_index_data = default_compressor.compress(b"".join(event_index_data))
        print("event_index_data", sum(map(len, event_index_data)), "->", len(compressed_event_index_data))
        fh.write(compressed_event_index_data)
        # n_fsrs
        fh.write(len(fsrs).to_bytes(length=2, byteorder="little", signed=False))
        # fsrs
        for fsr in fsrs:
            fh.write(default_compressor.compress(fsr))

        # HEADER
        # Go back and finish n_events and n_events_bytes
        fh.seek(24)
        fh.write(n_event.to_bytes(length=4, byteorder="little", signed=False))
        fh.write(offset.to_bytes(length=8, byteorder="little", signed=False))
