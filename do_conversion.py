#!/usr/bin/env python
import argparse
import json
import shutil
import time
from pathlib import Path

import xxhash

import decoding
import dst_convert
import encoding
import reversing


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(required=True)

    parser_to_cbf = subparsers.add_parser("to-cbf")
    parser_to_cbf.add_argument("input_path", type=Path)
    parser_to_cbf.add_argument("cbf_path", type=Path)
    parser_to_cbf.add_argument("--evt-max", type=int)
    parser_to_cbf.add_argument("--first-evt", type=int)
    parser_to_cbf.add_argument("--verbose", action="store_true")
    parser_to_cbf.add_argument("--without-side-channel", action="store_true")
    parser_to_cbf.add_argument("--export-size-info", type=Path)
    parser_to_cbf.add_argument("--compress-level", type=int, default=1)
    parser_to_cbf.add_argument("--compression-dicts-training-size", type=int, default=1_000)
    parser_to_cbf.add_argument("--only-physics", action="store_true")
    parser_to_cbf.set_defaults(
        func=lambda a: to_cbf(
            a.input_path, a.cbf_path, a.evt_max, a.first_evt, a.verbose, a.without_side_channel,
            a.export_size_info, a.compress_level, a.compression_dicts_training_size, a.only_physics
        )
    )

    subparser_read_cbf = subparsers.add_parser("read-cbf")
    subparser_read_cbf.add_argument("cbf_path", type=Path)
    subparser_read_cbf.add_argument("--export-size-info", type=Path)
    subparser_read_cbf.set_defaults(func=lambda a: read_cbf(a.cbf_path, a.export_size_info))

    subparser_to_mdf = subparsers.add_parser("to-mdf")
    subparser_to_mdf.add_argument("cbf_path", type=Path)
    subparser_to_mdf.add_argument("new_mdf_path", type=Path)
    subparser_to_mdf.add_argument(
        "--dstdata-compression-algorithm",
        type=reversing.DstDataCompressionAlgorithm,
        default=reversing.DstDataCompressionAlgorithm.ZSTD
    )
    subparser_to_mdf.add_argument("--no-preserve-bank-order", action="store_true")
    subparser_to_mdf.add_argument("--emulate-2024", type=reversing.Emulatable2024Streams)
    subparser_to_mdf.set_defaults(func=lambda a: to_mdf(a.cbf_path, a.new_mdf_path, a.dstdata_compression_algorithm, not a.no_preserve_bank_order, a.emulate_2024))

    subparser_list_raw_banks = subparsers.add_parser("list-raw-banks")
    subparser_list_raw_banks.add_argument("mdf_path", type=Path)
    subparser_list_raw_banks.set_defaults(func=lambda a: list_raw_banks(a.mdf_path))

    subparser_strip_overhead = subparsers.add_parser("strip-overhead")
    subparser_strip_overhead.add_argument("cbf_path", type=Path)
    subparser_strip_overhead.add_argument("output_path", type=Path)
    subparser_strip_overhead.set_defaults(func=lambda a: strip_overhead(a.cbf_path, a.output_path))

    args = parser.parse_args()
    return args.func(args)


def to_cbf(input_path, cbf_path, evt_max, first_evt, verbose, without_side_channel, export_size_info, compress_level, compression_dicts_training_size, only_physics):
    size_info = {} if export_size_info else None
    fsrs = []
    side_channel = None if without_side_channel else []

    start = time.perf_counter()
    if input_path.suffix in (".mdf", ".raw"):
        read_func = decoding.read_old_events
    elif input_path.suffix == ".dst":
        read_func = dst_convert.read_old_events
    else:
        raise NotImplementedError(input_path.suffix)
    old_events = read_func(
        input_path, evt_max=evt_max, first_evt=first_evt, verbose=verbose, side_channel=side_channel, fsrs=fsrs, size_info=size_info, only_physics=only_physics
    )
    encoding.write_new_events(cbf_path, old_events, fsrs, compress_level=compress_level, compression_dicts_training_size=compression_dicts_training_size)
    stop = time.perf_counter()

    side_channel_path = cbf_path.with_suffix(".sidechannel")
    side_channel_path.write_text(json.dumps(side_channel))
    if export_size_info:
        export_size_info.write_text(json.dumps(size_info))

    print("Converted", f"{len(side_channel)} events" if side_channel else "file", "in", stop - start, "seconds")


def read_cbf(cbf_path, export_size_info):
    size_info = {} if export_size_info else None

    start = time.perf_counter()
    reversing.read_cbf(cbf_path, size_info=size_info)
    stop = time.perf_counter()

    if export_size_info:
        export_size_info.write_text(json.dumps(size_info))

    print("Read", cbf_path, "in", stop - start, "seconds")


def to_mdf(cbf_path, new_mdf_path, dstdata_compression_algorithm, preserve_bank_order, emulate_2024):
    side_channel = json.loads(cbf_path.with_suffix(".sidechannel").read_text())

    start = time.perf_counter()
    reversing.read_cbf(cbf_path, side_channel=side_channel, mdf_out=new_mdf_path, dstdata_compression_algorithm=dstdata_compression_algorithm, preserve_bank_order=preserve_bank_order, emulate_2024=emulate_2024)
    stop = time.perf_counter()
    print("Wrote", new_mdf_path, "in", stop - start, "seconds")


def list_raw_banks(mdf_path):
    available_types = {(x.type, x.version) for event in decoding.read_old_events(mdf_path) for x in event.raw_banks}
    print(available_types)


def strip_overhead(cbf_path, output_path):
    with cbf_path.open("rb") as fh, output_path.open("wb") as out_fh:
        guid, n_events, n_events_bytes = reversing.read_header(fh)

        remaining = n_events_bytes
        while remaining:
            remaining -= out_fh.write(fh.read(min(remaining, 1024*1024)))

        assert fh.tell() == reversing.HEADER_SIZE + n_events_bytes, (fh.tell(), reversing.HEADER_SIZE + n_events_bytes)

        assert fh.read(8) == b"LHCbCBF\x00"
        n_compress_dicts = int.from_bytes(fh.read(4), byteorder="little", signed=False)
        hasher = xxhash.xxh64()
        for _ in range(n_compress_dicts):
            raw_dict_size = fh.read(4)
            hasher.update(raw_dict_size)
            dict_data = fh.read(int.from_bytes(raw_dict_size, byteorder="little", signed=False))
            hasher.update(dict_data)
        expected_checksum = int.from_bytes(fh.read(4), byteorder="little", signed=False)
        actual_checksum = int.from_bytes(hasher.digest()[:3:-1], byteorder="little", signed=False)
        assert actual_checksum == expected_checksum

        shutil.copyfileobj(fh, out_fh)


if __name__ == "__main__":
    parse_args()
