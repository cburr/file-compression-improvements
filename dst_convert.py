#!/usr/bin/env python3
import re
from typing import Iterator

import uproot
from tqdm import tqdm

import decoding


def endrot(data):
    out = bytearray(data)
    out[0::4] = data[3::4]
    out[1::4] = data[2::4]
    out[2::4] = data[1::4]
    out[3::4] = data[0::4]
    return out


def read_old_events(input_path, *, evt_max=None, first_evt=None, verbose=False, fsrs=None, side_channel=None, size_info=None, only_physics=False) -> Iterator[decoding.OldLHCbEvent]:
    # dst_path = Path("../data/00204721_00000045_1.bandq.mdf")
    # stream = "bandq"
    # mdf_path = dst_path.with_suffix(".dst")
    # dst_path = Path("../data/LHCb/Collision23/CHARM.DST/00198130/0000/00198130_00001605_1.charm.dst")
    # stream = "charm"
    if only_physics:
        raise NotImplementedError()

    f = uproot.open(input_path)
    stream = None
    for branch_name in f["Event"].keys():
        if match := re.fullmatch(r"_Event_([^_]+)_RawEvent.", branch_name):
            assert stream is None, f"Multiple streams found: {stream}, {match.groups()[0]}"
            stream = match.groups()[0]
    if stream is None:
        raise NotImplementedError("No stream found")

    branch = f["Event"][f"_Event_{stream}_RawEvent."]

    OBJECT_HEADER = b"\x40\x00\x00\x07\x00\x00\x83\x9b\x82\xe0\x00\x40"

    interpretation = uproot.interpretation.jagged.AsJagged(
        uproot.interpretation.numerical.AsDtype("u1")
    )
    BATCH_SIZE = 10_000
    print(branch, branch.compressed_bytes, branch.uncompressed_bytes)
    with tqdm(total=branch.num_entries) as pbar:
        n_event = 0
        for start in range(0, branch.num_entries, BATCH_SIZE):
            for event in branch.array(interpretation, entry_start=start, entry_stop=start+BATCH_SIZE, library="np"):
                n_event += 1
                if first_evt and n_event < first_evt:
                    continue
                data = event.tobytes()

                assert data.startswith(OBJECT_HEADER)
                object_size = int.from_bytes(data[0x0c:0x0e+1], "big")
                assert len(data) == object_size+15
                n_raw_banks = int.from_bytes(data[0x17:0x1a+1], "big")
                raw_bank_sizes = [4 * int.from_bytes(data[0x1b+i*4:0x1b+(i+1)*4], "big") for i in range(n_raw_banks)]
                offset = len(OBJECT_HEADER) + 3 + 7 + 4 + n_raw_banks*4 + 1
                assert 3 + len(OBJECT_HEADER) + object_size == offset + sum(raw_bank_sizes) + n_raw_banks
                raw_bank_data = []
                for raw_bank_size in raw_bank_sizes:
                    offset += 1
                    raw_bank_data.append(data[offset:offset+raw_bank_size])
                    offset += raw_bank_size
                # for raw_bank in decoding.decode_event(bytes(endrot(b"\x00"*4*12 + b"".join(raw_bank_data)))):
                #     # print(i, repr(raw_bank.type), raw_bank.full_size)
                #     pass
                raw_data = bytes(endrot(b"\x00"*4*12 + b"".join(raw_bank_data)))
                yield decoding.process_event(n_event, raw_data, is_physics_event=True, verbose=verbose, side_channel=side_channel, size_info=size_info)
                pbar.update(1)
